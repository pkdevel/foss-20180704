/* eslint-disable no-unused-expressions */
/* eslint-disable no-unused-vars */

import chai from 'chai'
import Asciidoctor from 'asciidoctor.js'
const should = chai.should()
var fs = require('fs')
var asciidoctor = Asciidoctor()

describe('Doc', () => {
  xit('should contain doc', (done) => {
    fs.readFile('./doc/install.adoc', 'utf8', function (err, data) {
      if (err) throw err
      var html = asciidoctor.convert(data)
      html.length.should.be.above(200)
      done()
    })
  })

  xit('should contain a toc', (done) => {
    fs.readFile('./README.adoc', 'utf8', function (err, data) {
      if (err) throw err
      data.should.contain(':toc:')
      done()
    })
  })
})
